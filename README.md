# README 🚀 #

Esta es una prueba de habilitar a travez de **NodeJS** y **Express** un pull de API's que permitan hacer uso de diversos métodos como GET, POST, DELETE.

Versión 1.0 Erick Venegas Toledo **erick.venegas.toledo@gmail.com**

### NodeJS 🛠️ ###

* Se utiliza la versión 12.16.1 de NodeJS. Para exponer API's se utiliza Express, este es un framework para NodeJS.

### ORM Sequalize 🛠️ ###

* El **ORM Sequalize** nos perite manipular una base de datos MySQL. Sin dejar de mencionar que un ORM es un modelo de programación que permite mapear las estructuras de una base de datos relacional.

### Consideraciones 📋 ###

* Se debe crear una base de datos en MySQL, debe ser creada con el mismo nombre que se encuentra en el archivo de configuración del proyecto (config.json), en este caso sería **ormnode**. Una vez que se crea la base de datos se debe inicializar backend.
* Se debe considerar que al inicializar backend se crearán las tablas en la base de datos y en la tabla usuarios, se creará por defecto un usuario administrador **admin@admin.com** y su password **12345678**

### Inicializar Backend 🔧 ###

* Luego de descargar del repositorio el proyecto se debe inicializar.
* npm install
* nodemon app.js

### Documentación Postman 🚀 ###

* **GET http://localhost:3001/auth/login**

    ```
    {
        "usuario": "admin@admin.com",
        "clave": "12345678"
    }
    ```

* **GET http://localhost:3001/usuarios**

* **POST http://localhost:3001/auth/registro**

    ```
        {
            "usuario": "admin@admin.cl",
            "clave": "123456",
            "nombres": "Erick",
            "apellidos": "Venegas Toledo",
            "rut": "15677877-K",
            "telefono": "978346100",
            "correo": "erick@gmail.com",
            "tiposUsuarioIdTiposUsuarios": 1,
            "borrado": 0
        }
    ```

* **GET http://localhost:3001/categorias**

* **POST http://localhost:3001/categorias**

    ```
        {
            "nombre_categoria": "Categoría 88",
            "borrado": "0",
            "nombre_id": ""
        }
    ```

* **POST http://localhost:3001/categorias/2**

    ```
        {
            "nombre_categoria": "Categoría 80",
            "borrado": "0",
            "nombre_id": ""
        }
    ```

* **DELETE http://localhost:3001/categorias/2**
