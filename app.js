
const express = require("express");
const jwt = require('jsonwebtoken');
const config = require('./config/config');
const db = require("./models");
const api = require('./routes/index.routes');
const init = require("./controllers/default/init.controller");
const jwtMiddleware = require("./middlewares/jwt");
const socket = require('./controllers/web-socket/socket');

const app = express();
const cors = require('cors');
const http = require("http");
const path = require("path");
const fs = require("fs");
const multer = require("multer");

const upload = multer({
  dest: "./uploads"
});

app.use(cors({ origin: "*" }));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

app.set('llave', config.llave);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static("app/public"));

const protegerRutas = jwtMiddleware.protegerRutas(app, express, jwt);

api(app, db, protegerRutas, upload);

db.sequelize.sync().then(() => {
  app.listen(3001, () => console.log('¡API escuchando en el puerto 3001!'));
  init.verificar(db);
  socket.init(app);
});

app.use('/uploads', express.static('uploads'));
app.get('/', express.static(path.join(__dirname, './uploads')));
