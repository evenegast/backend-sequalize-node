const messages = require('../default/messages.controller');

module.exports = {
  obtener_usuario: async (db,req,res) => {
    const randomDigit = req.params.random_digit;

    const usuario = await db.usuarios.findOne({ where:{ random_digit: randomDigit } });

    messages.success(usuario,res)
  }
}
