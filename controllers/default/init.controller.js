module.exports = {
    verificar: async (db) => {
        const tipos_usuarios = await db.tipos_usuarios.findAll();
        tipos_usuarios.length === 0 ? inicializar(db) : null;
    }
}

const inicializar = async(db) => {
    const tipo_persona = {
        tipo_usuario: 'Cliente',
        borrado: 0
    }

    const tipo_empresa = {
        tipo_usuario: 'Emprendedor',
        borrado: 0
    }

    const tipo_admin = {
        tipo_usuario: 'Administrador',
        borrado: 0
    }

    const tipo_postulacion = {
        tipo_usuario: 'Postulacion',
        borrado: 0
    }

    const usuario_admin = {
        clave: '$2a$10$a4JzBQ1c/VLBSLgdVJ3X9uo7/oUSFBvnycRiv4GQS1DppK1EGn7KK',
        correo: 'admin@admin.com',
        tiposUsuarioIdTiposUsuarios: 3,
        borrado: 0
    }

    await db.tipos_usuarios.create(tipo_persona).then(result => console.log("Tipo de usuario: Usuario. Insertado")).catch(err => console.log(err));
    await db.tipos_usuarios.create(tipo_empresa).then(result => console.log("Tipo de usuario: Empresa. Insertado")).catch(err => console.log(err));
    await db.tipos_usuarios.create(tipo_admin).then(result => console.log("Tipo de usuario: Administrador. Insertado")).catch(err => console.log(err));
    await db.tipos_usuarios.create(tipo_postulacion).then(result => console.log("Tipo de usuario: Administrador. Insertado")).catch(err => console.log(err));
    await db.usuarios.create(usuario_admin).then(result => console.log("Usuario admin 12345678 insertado.")).catch(err => console.log(err));
}