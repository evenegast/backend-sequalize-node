const nodemailer = require("nodemailer");
const messages = require('../default/messages.controller');
const config = require('../../config');

const transporter = nodemailer.createTransport({
  host: 'smtp.gmail.com',
  port:'465',
  auth: {
    user: 'erick.venegas.toledo@gmail.com', //< colocar usuario gmail con bajo nivel de seguridad
    pass: '123456' //< password de ese user
  }
});

module.exports = {
  postulacion_rechazada: (usuario, description, res) => {
    // Definimos el email
    const mailOptions = {
      from: "erickvader@gmail.com", //< mismo correo definido arriba
      to: usuario.correo, //< correo al que se le va a enviar el mensaje
      subject: "Test MarketPlace",
      text:`Buenas ${usuario.razon_social}. Su postulación ha sido revisada por nuestro equipo, y ha sido rechazada por el siguiente motivo. ${description}`
    };

    // Enviamos el email
    transporter.sendMail(mailOptions).then(function (email) {
      messages.success('Message send',res);
    }).catch(function (exception) {
      console.log('exception ->', exception);
      messages.error(exception, res);
    });
  },

  postulacion_aceptada: (table,req,res) => {
    console.log('req.body ->', req.body);

    const usuario = req.body;
    const randomDigit = randomStr(10,'12345abcde');

    console.log('randomDigit ->', randomDigit);

    table.update({ random_digit: randomDigit, tiposUsuarioIdTiposUsuarios: 2 }, { where: { id_usuarios: usuario.id_usuarios } });

    const mailOptions = {
      from: "ericktest@gmail.com", //< mismo correo definido arriba
      to: usuario.correo, //< correo al que se le va a enviar el mensaje
      subject: "Test MarketPlace",
      text: `Buenas ${usuario.razon_social}. Su postulación ha sido revisada por nuestro equipo, y ha sido aceptada, por el siguiente link puede continuar su registro. ${config.URL_FRONT}/completar-registro/${randomDigit}`
    };

    // Enviamos el email
    transporter.sendMail(mailOptions).then(function (email) {
      messages.success('Message send', res);
    }).catch(function (exception) {
      console.log('exception ->', exception);
      messages.error(exception, res);
    });
  }
}

function randomStr (len, arr) { 
  let ans = '';

  for (let i = len; i > 0; i--) { 
    ans += arr[Math.floor(Math.random() * arr.length)]; 
  }

  return ans; 
}
