module.exports = (sequelize, DataTypes) => {
  const Productos = sequelize.define('productos', {
    id_productos: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    nombre_producto:{
      type: DataTypes.STRING,
    },
    precio:{
      type: DataTypes.INTEGER,
    },
    stock:{
      type: DataTypes.INTEGER,
    },
    descripcion_producto:{
      type: DataTypes.STRING,
    },
    sucursal:{
      type:DataTypes.STRING,
    },
    bsale:{
      type:DataTypes.STRING,
    },
    borrado:{
      type: DataTypes.INTEGER,
    },
  },
  {
    freezeTableName: true
  });

  Productos.associate = (models) => {
    Productos.belongsTo(models.categorias);
    Productos.hasMany(models.pagos);
  };

  return Productos;
}
