const authLogin = require('../controllers/auth/login.controller');
const authRegistro = require('../controllers/auth/registro.controller');
const authRecuContra = require('../controllers/auth/recuperar_contasena.controller');
const route = "/auth";
const crud = require('../controllers/default/crud.controller');

module.exports = (app,db) => {
    app.post(`${route}/login`, (req, res) => authLogin.login(req, res, db));
    app.post(`${route}/registro`, (req,res) => authRegistro.registro(req, res, db));
    
    app.post(`${route}/postulacion`, (req,res) => crud.create(db.usuarios, req, res));

    app.post(`${route}/recupera_contra`, (req, res) => authRecuContra.olvide_contra(req, res, db));
    app.post(`${route}/verifica_token`, (req, res) => authRecuContra.verificaToken(req, res, db));
    app.post(`${route}/cambia_contra`, (req, res) => authRecuContra.cambiaContra(req, res, db));
}
