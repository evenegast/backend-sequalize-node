const auth = require('./auth.routes');
const usuarios = require('./usuarios.routes');
const categorias = require('./categorias.routes');
const productos = require('./productos.routes');
const correos = require('./correos.routes')

module.exports = (app, db, protegerRutas, multer) => {
    auth(app, db);
    usuarios(app, db);
    categorias(app, db);
    productos(app, db, multer);
    correos(app, db);
}
